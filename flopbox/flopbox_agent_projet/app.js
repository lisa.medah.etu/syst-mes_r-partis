
var express = require('express');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var serversRouter = require('./routage/servers')
var usersRouter = require('./routage/users');
var ftpRouter = require('./routage/ftpCmd');
var app = express();
app.use(bodyParser.json())

app.use(cookieParser());


app.use('/', usersRouter);
app.use('/', serversRouter);
app.use('/', ftpRouter);

app.listen(6060)
